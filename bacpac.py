import sys
import pandas as pd
import subprocess as sprc
from pathlib import Path

COLUMNS = ["PACKAGE", "DESCRIPTION"]
DIRECTORY = Path(sys.argv[1]) / "bacpac"


class PackageManager:
    def __init__(self, name, installer, lister, updaters, filename=None, list_creator=None):
        self.name = name
        self.installer = ['sudo'] + installer
        self.lister = lister
        self.updaters = updaters
        if filename is None:
            self.path = DIRECTORY / ("%s.csv" % self.name)
        else:
            self.path = DIRECTORY / filename
        if self.path.is_file():
            self.table = pd.read_csv(self.path)[COLUMNS] # Kan hende jeg bør legge inn en feilhåndtering på hva som skjer om kollonnene har feil navn.
        else:
            self.table = self._make_empty_table()
        if list_creator is None:
            self.list_creator = lambda text: text.split("\n")
        else:
            self.list_creator = list_creator
        #self.packages = self._make_packagelist(self.filename)

    @staticmethod
    def _make_empty_table():
        new_empty_table = pd.DataFrame(columns=COLUMNS)
        return new_empty_table

    @staticmethod
    def _make_new_row(package_name, description):
        new_row = pd.DataFrame({COLUMNS[0]: [package_name], COLUMNS[1]: [description]})
        return new_row

    def add_package(self, package_name, description):
        new_table = self._make_new_row(package_name, description)
        self.table.append(new_table)

    @property
    def packagelist(self):
        result = []
        for package in self.table["PACKAGE"]:
            if package == package:
                package_str = str(package)
                result.append(package_str)
        return result

    def add_package(self, new_package):
        #print(new_package, type(new_package), new_package.decode("utf-8"), type(new_package.decode("utf-8")))
        self.table.loc[len(self.table)] = [new_package.decode("utf-8"), ""]

    def update_os(self):
        for updater in self.updaters:
            sprc.run(updater)

    def update_table_from_os(self):
        for package in self.list_creator(sprc.run(self.lister, capture_output=True).stdout):
            if not package in self.packagelist:
                self.add_package(package)

    def install_packages_from_list(self):
        #print(self.installer)
        cmd = self.installer + self.packagelist
        #print(cmd)
        sprc.run(cmd)

    def write_package_list(self):
        self.path.parent.mkdir(parents=True, exist_ok=True)
        with open(self.path, 'w') as outfile:
            self.table.to_csv(self.path, index=False)


def modify_apt_list(text):
    lines = text.split(b'\n')
    packages = []
    for line in lines:
        packages.append(line.replace(b'\t',b' ').split(b' ')[0])
    return packages


pacman = PackageManager(name='pacman', installer=['pacman','-S'], lister=['pacman','-Qqe'], updaters=[['pacman','-Syu']])
apt = PackageManager(name='apt', installer=['apt','install'], lister=['dpkg','--get-selections'], updaters=[['apt','update'],['apt','upgrade']], list_creator=modify_apt_list)

#apt.update_table_from_os()
#apt.write_package_list()
#apt.install_packages_from_list()
apt.update_os()
